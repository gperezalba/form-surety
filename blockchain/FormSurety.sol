pragma solidity 0.5.0;

import "./Owned.sol";
import "./safeMath.sol";

/// @author MIDLANTIC TECHNOLOGIES
/// @title Contract designed to handle Sureties for SUNAVAL's Forms

contract FormSurety is Owned {
    using SafeMath for uint;
    
    struct Key {
        address payable owner;
        uint price;
        bool exist;
        bool payedBack;
    }
    
    mapping(bytes32 => Key) public keys;
    
    uint[] public prices;
    
    constructor() public {
        
    }
    
    event NewPrice(uint indexed newPrice, uint indexed category, uint[] prices);
    event NewForm(bytes32 indexed key, uint indexed category, address indexed owner, uint securety);
    event PayBack(bytes32 indexed key, address indexed owner, uint securety);
    
    function newForm(bytes32 key, uint category) external payable {
        require(msg.value == prices[category]);
        require(!keys[key].exist);
        
        keys[key].owner = msg.sender;
        keys[key].price = msg.value;
        keys[key].exist = true;
        
        emit NewForm(key, category, msg.sender, msg.value);
    }
    
    function setNewPrice(uint newPrice) public onlyOwner {
        prices.push(newPrice);
        
        emit NewPrice(newPrice, prices.length.sub(1), prices);
    }
    
    function changePrice(uint newPrice, uint category) public onlyOwner {
        prices[category] = newPrice;
        
        emit NewPrice(newPrice, category, prices);
    }

    function payBack(bytes32 verifiedKey) public onlyOwner {
        require(!keys[verifiedKey].payedBack);
        keys[verifiedKey].payedBack = true;
        keys[verifiedKey].owner.transfer(keys[verifiedKey].price);
        
        emit PayBack(verifiedKey, keys[verifiedKey].owner, keys[verifiedKey].price);
    }
    
    function wrongForm(bytes32 verifiedKey) public onlyOwner {
        require(!keys[verifiedKey].payedBack);
        keys[verifiedKey].payedBack = true;
        msg.sender.transfer(keys[verifiedKey].price);
        
        emit PayBack(verifiedKey, msg.sender, keys[verifiedKey].price);
    }
}