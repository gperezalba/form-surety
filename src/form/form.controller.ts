import { Controller, Post, Get } from '@nestjs/common';
import { FormService } from './form.service';

@Controller('form')
export class FormController {
    constructor(private readonly formService: FormService) {}

    @Post('checkForm')
    async checkForm(form: any, formKey: string): Promise<any> {
        let receipt = await this.formService.checkForm(form, formKey);

        return receipt;
    }
}
