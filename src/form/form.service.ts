import { Injectable } from '@nestjs/common';

const Web3 = require('web3');
const Tx = require('ethereumjs-tx').Transaction;
const Common = require('ethereumjs-common');
const BigNumber = require('bignumber.js');

const formABI = [{"constant":false,"inputs":[{"name":"_new","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"verifiedKey","type":"bytes32"}],"name":"payBack","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"key","type":"bytes32"},{"name":"category","type":"uint256"}],"name":"newForm","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"newPrice","type":"uint256"},{"name":"category","type":"uint256"}],"name":"changePrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"prices","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"verifiedKey","type":"bytes32"}],"name":"wrongForm","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"keys","outputs":[{"name":"owner","type":"address"},{"name":"price","type":"uint256"},{"name":"exist","type":"bool"},{"name":"payedBack","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newPrice","type":"uint256"}],"name":"setNewPrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"newPrice","type":"uint256"},{"indexed":true,"name":"category","type":"uint256"},{"indexed":false,"name":"prices","type":"uint256[]"}],"name":"NewPrice","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"key","type":"bytes32"},{"indexed":true,"name":"category","type":"uint256"},{"indexed":true,"name":"owner","type":"address"},{"indexed":false,"name":"securety","type":"uint256"}],"name":"NewForm","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"key","type":"bytes32"},{"indexed":true,"name":"owner","type":"address"},{"indexed":false,"name":"securety","type":"uint256"}],"name":"PayBack","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"old","type":"address"},{"indexed":true,"name":"current","type":"address"}],"name":"NewOwner","type":"event"}];
const formADDRESS = "0x733c54d63dd6f5d12386bdac4c8e3e2ffe506516";

@Injectable()
export class FormService {
    private web3: any = new Web3(new Web3.providers.HttpProvider("http://81.46.240.151:8545"));
    private customCommon: any = Common.default.forCustomChain('mainnet', { name: 'Pi-Chain', networkId: 8995, chainId: 8995 }, 'petersburg');
    private formContract: any = new this.web3.eth.Contract(formABI, formADDRESS);
    private account: string = "0xff22373340A43CB4b4Ef9697a65f6e19c1F7c892"; //pillar de metamask en front

    // Esta irá en el front pero te la pongo para que veas el proceso de crear la formKey
    // IMPORTANTE: EL FORMULARIO TIENE QUE TENER ALGUN ID ÚNICO, LA BLOCKCHAIN NO VA A 
    // ACEPTAR 2 HASH/KEY IGUALES
    async newForm(form: any, category: number): Promise<any> {
        let formStringified = JSON.stringify(form);
        let formKey = this.web3.utils.sha3(formStringified);
        let price = await this.formContract.methods.prices(String(category)).call();
        let estimatedGas = await this.formContract.methods.newForm(formKey, String(category)).estimateGas({from: this.account, value: price});
        let receipt = await this.formContract.methods.newForm(formKey, String(category)).send({from: this.account, value: price, gas: estimatedGas});

        return receipt;
    }

    async getKey(formKey: string): Promise<any> {
        let formBlockchain = await this.formContract.keys(formKey).call();
        return formBlockchain;

        // [0] - string owner
        // [1] - string fianza
        // [2] - boolean existe/noexiste
        // [3] - boolean deuvelto/no devuelto
    }

    async checkForm(form: any, formKey: string): Promise<any> {
        let formStringified = JSON.stringify(form);
        let sentFormKey = this.web3.utils.sha3(formStringified);
        let status = false;
        let receipt: any;

        if (formKey == sentFormKey) {
            // Todo lo que tú veas que hay que hacer aquí...
            // Si quieres comprobar que en blockchain está correcto:
            let formBlockchain = await this.getKey(formKey);
            console.log(formBlockchain);

            while(!status) {
                receipt = await this.payBack(formKey);
                status = receipt.status;
            }
            
            return receipt;
        }
    }

    // Devuelve al usuario su fianza
    async payBack(formKey: string): Promise<any> {
        let status = false;
        let receipt: any;

        let calldata = await this.formContract.methods.payBack(formKey).encodeABI();

        while(!status) {
            receipt = await this.sendTx(calldata, formADDRESS);
            status = receipt.status;
        }

        return receipt;
    }

    // Nos envía a nosotros la fianza
    async wrongForm(formKey: string): Promise<any> {
        let status = false;
        let receipt: any;

        let calldata = await this.formContract.methods.wrongForm(formKey).encodeABI();

        while(!status) {
            receipt = await this.sendTx(calldata, formADDRESS);
            status = receipt.status;
        }

        return receipt;
    }

    async sendTx(calldata: string, contractAddress: string): Promise<any> {
        let privateKey = Buffer.from('9F47B7FDC796C13251731827CC59E0083E5BEC070E7F53EADD54D1DD2BBDBF17', 'hex');
        let signer = "0xff22373340A43CB4b4Ef9697a65f6e19c1F7c892";

        let nonce = await this.web3.eth.getTransactionCount(signer);
        let nonceHex = this.web3.utils.toHex(nonce);
        const gasPrice = 0;
        const gasPriceHex = this.web3.utils.toHex(gasPrice);
        const gasLimit = 6000000;
        const gasLimitHex = this.web3.utils.toHex(gasLimit);

        let tx = new Tx(
            {
            nonce: nonceHex,
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            to: contractAddress,
            value: '0x00',
            data: calldata
            },
            { common: this.customCommon },
        )

        tx.sign(privateKey);
        let serializedTx = tx.serialize();
        let receipt = await this.web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));//.on('receipt', console.log);
        return receipt;
    }
}
